# Companion for IEEE ISC2 2022 Conference paper

IEEE ISC2 2022, 8th IEEE International Smart Cities Conference 2022, 26-29 September 2022, Aliathon Resort, Paphos, Cyprus https://attend.ieee.org/isc2-2022/

## Reference

Please cited : Louis Closson, Christophe Cérin, Didier Donsez and Denis Trystram, Towards a Methodology for the Characterization of IoT Data Sets of the Smart Building Sector, IEEE ISC2 2022, 8th IEEE International Smart Cities Conference 2022, 26-29 September 2022, Aliathon Resort, Paphos, Cyprus https://attend.ieee.org/isc2-2022/

## Full report

Coming soon on HAL

## Dataset
Delinchant B., Wurtz F., Ploix S., Schanen J.-L. and Marechal Y. (2016). GreEn-ER Living Lab - A Green Building with Energy Aware Occupants. SmartGreen'16, In proceedings of the 5th International Conference on Smart Cities and Green ICT Systems. ISBN 978-989-758-184-7, pages 316-323. [DOI: 10.5220/0005795303160323](https://www.researchgate.net/publication/302974016_GreEn-ER_Living_Lab_-_A_Green_Building_with_Energy_Aware_Occupants)

GreEn-ER OpenData API: https://mhi-srv.g2elab.grenoble-inp.fr/django/API/

[Scripts](./dataset)

