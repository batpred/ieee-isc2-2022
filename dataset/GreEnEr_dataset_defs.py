# -*- coding: utf-8 -*-
"""
Created on Thu May  5 11:53:22 2022

@author: Louis Closson - louis.closson-professional@outlook.fr
"""

import pandas as pd
import os
import json
from pandas.io.json import json_normalize
import numpy as np
import matplotlib.pyplot as plt
import csv
import copy





def df_weather_parser(weather_df, timestep=10):
    timestep = timestep*60000
    weather_df.sort_values(by = ['timestamp'], inplace=True)
    #minutes of beginning and end, considering the timestep :
    begining_date = weather_df['timestamp'].min()
    #end_date      = weather_df['timestamp'][len(weather_df)-1]
    end_date      = weather_df['timestamp'].max()
    minute_beginning  = (begining_date // timestep + 1)*timestep
    minute_ending     = (end_date      // timestep    )*timestep
    #select wanted timestamps - each [timestep] minute(s)
    print(begining_date, end_date)
    nb_ts        = int( (minute_ending - minute_beginning) / timestep )
    print("nb_ts :", nb_ts )
    parsed_tab = np.zeros((nb_ts, 10))
    #creation of parsed tab, wthout values
    for i_ts in range(nb_ts):
        parsed_tab[i_ts][0] = minute_beginning + i_ts*timestep
    #add values
    i = 0
    current_Mat = weather_df.to_numpy()
    for j in range( len(parsed_tab)): #by building : all ts of index j have a ts of index i after and before them
        for i_feature in range(1,10):
            while current_Mat[i+1][0] < parsed_tab[j][0] or current_Mat[i][0] >  parsed_tab[j][0]: #we want the following raw timestamp
                i = i+1
            growth = (current_Mat[i+1][i_feature]-current_Mat[i][i_feature]) / (current_Mat[i+1][0]-current_Mat[i][0])
            parsed_tab[j][i_feature] = current_Mat[i][i_feature] + (parsed_tab[j][0]-current_Mat[i][0]) * growth
    # Change into dataframe
    cols = weather_df.columns
    weather_df = pd.DataFrame(parsed_tab, columns = cols)
    return weather_df





def Json_to_weather_df(path=None, timestep=10):
    '''
    

    Parameters
    ----------
    path : TYPE, optional
        DESCRIPTION. The default is None. Path from where the main script calling this function is written.
    timestep : TYPE, optional
        DESCRIPTION. The default is 10.Time in minutes bewteen samples.

    Returns
    -------
    weather_df : pandas.dataframe
    

    '''
    if not path:
        name=input("name of the date file (with .type at the end): ")
        path=os.getcwd()
        path=path.replace("\\","/")
        path=path+"/"+name
    f=open(path)
    txt=f.read()
    liste=txt.split("}}")
    liste.pop(-1)
    for i in range(len(liste)):
        liste[i]=liste[i]+"}}"
        liste[i]=liste[i].split(";MSG/greener/dashboard;")
        if len(liste[i])!=2:#i==3560 :
            #print(liste[i])
            liste[i]=[liste[i][0],liste[i][-1]]
        liste[i][0]='{"timestamp": '+str(int(liste[i][0]))
        liste[i][1]=liste[i][1].replace('{', '', 1)
        liste[i]=",".join(liste[i])

    x = json.loads(liste[0])
    cols=[]
    timestamp = ['timestamp']
    cols += timestamp
    sunc       = list(pd.DataFrame(x['sun'], index=[0]).columns)
    cols += sunc
    weatherc   = list(pd.DataFrame(x['weather'], index=[0]).columns)
    weatherc=weatherc[:3] #last index in always Nan - we delete it
    cols += weatherc
    windc      = list(pd.DataFrame(x['wind'], index=[0]).columns)
    cols += windc

    #cols = ['timestamp', 'sun', 'weather', 'wind'] #not working - need more columns
    weather_df = pd.DataFrame(index=np.arange(len(liste)),columns=cols)
    #weather_df.columns=pd.MultiIndex.from_arrays([timestamp,sun, weather, wind])

    for i in range(len(liste)):
        x = json.loads(liste[i])
        # timestamp = x['timestamp']
        # sun       = pd.DataFrame(x['sun'], index=[0])
        # weather   = pd.DataFrame(x['weather'], index=[0])
        # wind      = pd.DataFrame(x['wind'], index=[0])
        
        weather_df.iloc[i]['timestamp'] = x['timestamp']
        
        weather_df.iloc[i]['direct']    = x['sun']['direct']
        weather_df.iloc[i]['diffuse']   = x['sun']['diffuse']
        weather_df.iloc[i]['global']    = x['sun']['global']
        
        weather_df.iloc[i]['tempCurrent']    = x['weather']['tempCurrent']
        weather_df.iloc[i]['tempMin']    = x['weather']['tempMin']
        weather_df.iloc[i]['tempMax']    = x['weather']['tempMax']
        
        weather_df.iloc[i]['direction']    = x['wind']['direction']
        weather_df.iloc[i]['speed']    = x['wind']['speed']
        weather_df.iloc[i]['peak']    = x['wind']['peak']
        
    weather_df = df_weather_parser (weather_df, timestep)
        
    return weather_df






def load_or_create_weatherdf(load_path, timestep):
    path=os.getcwd()
    path=path.replace("\\","/")
    load_path = path + load_path
    suffix=load_path[-12:-4] #date
    save_name = 'weather_df_'+suffix+'.'+str(timestep)+'min.csv'
    df_path = path+"/logAndCSV_GreEnEr/"+save_name
    try : #try to load
        weather_df = pd.read_csv(df_path)
        cols=['timestamp','direct','diffuse','global','tempCurrent','tempMin','direction','speed','peak']
        print("weather data successfully loaded !")
    except : #if not possible : try to create and save
        #create
        timestep = 10 # in minutes between samples
        # #path=path+"/GreEn-Er data.json"
        # #path=path+"/GreEnEr_energy_data.json"
        weather_df = Json_to_weather_df(load_path,timestep)
        #save
        weather_df.to_csv(df_path, index_label=False)
        #load - because there is some nice colours (only after a loading...)
        weather_df = pd.read_csv(df_path)
        print("weather data successfully created, saved and loaded")
    return weather_df



def df_comfort_parser(comfort_df, feature=2, timestep=10, same_nb_sensors=False):
    ''' It take a dataframe with rooms names in column 1 and only keep timestamps where all rooms exist.
        timestep is the time between timestamps, given in minutes
        We chose to fill lacking data with average of the timestamps before.
        feature=2 for temperature and feature=3 for CO2
    '''
    timestep = timestep*60000 #express it in millisecondes like in raw data
    #remove malfunctionning co2 sensors
    current_df = copy.copy(comfort_df)
    current_df = current_df[current_df.loc[:][1] != "2D-003"] #high  value outlier
    current_df = current_df[current_df.loc[:][1] != "5D-007"] #low value outlier
    current_df = current_df[current_df.loc[:][1] != "4C-008"] #sensor down during studying time (early study)
    

    # case of all timestamp of all rooms in 1 column
    groups = current_df.groupby(current_df.columns[1]) #group by room
    end_date = groups[0].max().min() #minimal date (of last timestamps) to use
    current_df = current_df[current_df.loc[:][0] <= end_date] #cut data to end date
    begining_date = groups[0].min().max() #maximal date (of first timestamps) to use
    current_df = current_df[current_df.loc[:][0] >= begining_date] #cut data to end date
    
    #minutes of beginning and end, considering the timestep :
    minute_beginning  = (begining_date // timestep + 1)*timestep
    minute_ending     = (end_date      // timestep    )*timestep
    
    #drop useless columns - column with only NaN (last check : 01/31/2022)
    current_df = current_df.drop(columns=[x for x in comfort_df.columns if (x>3) ])
    #drop Nan left in columns of interest
    if not same_nb_sensors:     # keep co2 OR temperature - chosen by 'feature'
        current_df = current_df.drop(columns=[x for x in current_df.columns if ((x>1) & (x!=feature)) ])
        current_df =current_df.dropna() #drop rows with NaN
    else:                       # keep sensors giving temperature AND co2
        current_df =current_df.dropna() #drop rows with NaN
        current_df = current_df.drop(columns=[x for x in current_df.columns if ((x>1) & (x!=feature)) ])
    
    #sort by room, then by timestamp
    current_df.sort_values(by = [1,0], inplace=True)
    
    
    #select wanted timestamps - each [timestep] minute(s)
    nb_sensors   = len(current_df.groupby(current_df.columns[1]).size())
    nb_ts        = int((minute_ending - minute_beginning) / timestep)
    parsed_tab = np.zeros((nb_ts*nb_sensors, 3))
    liste_sensors = []
    
    #creation of parsed tab, wthout values
    for i_sensor in range(nb_sensors):
        if i_sensor % 50 == 0:
            print(i_sensor)
        liste_sensors.append(current_df.groupby(current_df.columns[1]).size().index[i_sensor])
        for i_ts in range(nb_ts):
            parsed_tab[i_sensor*nb_ts + i_ts][0] = minute_beginning + i_ts*timestep
            parsed_tab[i_sensor*nb_ts + i_ts][1] = i_sensor #just a number for now, because string not supported by numpy
    
    #add values
    i = 0
    current_Mat = current_df.to_numpy()
    for j in range( len(parsed_tab)): #by building : all ts of index j have a ts of index i after and before them
        if (j/nb_ts)%50 == 0:  #print all 50 sensor being treated - for monitoring
            print(j/nb_ts)
        while current_Mat[i+1][0] < parsed_tab[j][0] or current_Mat[i][0] >  parsed_tab[j][0]: #we want the following raw timestamp
            i = i+1
        growth = (current_Mat[i+1][2]-current_Mat[i][2]) / (current_Mat[i+1][0]-current_Mat[i][0])
        parsed_tab[j][2] = current_Mat[i][2] + (parsed_tab[j][0]-current_Mat[i][0]) * growth
    
    # change the matrix to dataframe and add the real names of the rooms
    if feature==2:
        feature='temperature'
    elif feature==3:
        feature='co2'
    current_df = pd.DataFrame(parsed_tab, columns = ['timestamp','room',feature])
    current_df['room'] = current_df['room'].apply(lambda x : liste_sensors[int(x)])
    
    #change to 1 room per column
    groups = current_df.groupby('room') #group by room
    ts_length = groups.size().min() #number of timestamps per room - all equal now
    TimeSeries_dataset = np.zeros(( nb_sensors+1 , ts_length )) #+1 is for timestamps
    labelList = ['timestamp']
    current_Mat = current_df.to_numpy()
    TimeSeries_dataset[0] = current_Mat[ 0 : ts_length , 0]
    for i in range(nb_sensors) :
        TimeSeries_dataset[i+1] = current_Mat[ ts_length*i : ts_length*(i+1) , -1]
        labelList.append(current_Mat[i*ts_length,1])
        if feature==2:
            labelList[-1] = labelList[-1] + "-T°"
        elif feature==3:
            labelList[-1] = labelList[-1] + "-co2"
    TimeSeries_dataset = np.array(TimeSeries_dataset).T
    current_df=pd.DataFrame(TimeSeries_dataset, columns=labelList)
    return current_df
    
def load_or_create_comfortdf(load_path, timestep, feature=2):
    '''
    

    Parameters
    ----------
    load_path : str
        path from code file : not the absolute path !
    timestep : int, optional
        Number of minute(s) you desire between timestamps (minimum : 1min)
    feature : int, optional
        The default is 2. 2 is to take temperature feature. 3 is to take CO2 feature.

    Returns
    -------
    comfort_df : pandas.dataframe
    
    NOTE : prefer the use of the general parser
        

    '''
    path=os.getcwd()
    path=path.replace("\\","/")
    load_path = path + load_path
    suffix=load_path[-12:-4] #date
    if feature == 2:
        suffix ='temp_' + suffix
    if feature == 3:
        suffix = 'co2_' + suffix
    save_name='comfort_df_'+suffix+'.'+str(timestep)+'min.csv'
    df_path = path+"/logAndCSV_GreEnEr/"+save_name
    try : #try to load
        comfort_df = pd.read_csv(df_path)
        print("comfort data successfully loaded !")
    except : #if not possible : try to create and save
        #create
        # feature :2 for temperature / 3 for co2
        # timestep (in minutes): time between samples
        raw_comfort_df = pd.read_csv(load_path, header=None)
        current_df = df_comfort_parser(raw_comfort_df, feature=feature, timestep=timestep, same_nb_sensors=False)
        #save
        current_df.to_csv(df_path, index_label=False, header=True)
        #load - because there is some nice colours (only after a loading...)
        comfort_df = pd.read_csv(df_path)
        print("comfort data successfully parsed, saved and loaded !")
    return comfort_df




def general_parser(df, timestep, granularity_ts='millisec'): # in minutes
    '''
    
    
    Parameters
    ----------
    df              : pandas.dataframe
        dataframe - proven compatibility if pandas dataframe
    timestep        : int
        Number of minute(s) you desire between timestamps (minimum : 1min)
    granularity_ts  : str, optional
        The default is 'millisec'. 'sec' can be used. describes if timestamp is a 
        total number of milliseconds or a total number of seconds. Node-red may
        provide the milliseconds accuracy.

    Returns
    -------
    comfort_df : pandas.dataframe
        Parsed data at selected timestep
    
    

    '''
    timestep = timestep*60000
    if granularity_ts != 'millisec': #millisec
        if granularity_ts == 'sec':
            df['timestamp']=df['timestamp'].apply(lambda x :x*1000)
    df.sort_values(by = ['timestamp'], inplace=True)
    
    #minutes of beginning and end, considering the timestep :
    begining_date = df['timestamp'].min()
    end_date      = df['timestamp'].max()
    minute_beginning  = (begining_date // timestep + 1)*timestep
    minute_ending     = (end_date      // timestep    )*timestep
    
    #select wanted timestamps - each [timestep] minute(s)
    nb_features = len(df.iloc[0])
    cols = list(df.columns)
    nb_ts = int((minute_ending - minute_beginning) // timestep)
    print("number of lines :",nb_ts)
    parsed_tab = np.zeros((nb_ts, nb_features))
    
    #creation of parsed tab, without values
    for i_ts in range(nb_ts):
        parsed_tab[i_ts][0] = minute_beginning + i_ts*timestep
    
    #add values
    i = 0
    current_Mat = df.to_numpy()
    for j in range( len(parsed_tab)): #by building : all ts of index j have a ts of index i after and before them
        if j%1000 == 0:
            print("line", j, "traited")
        for i_feature in range(1,nb_features):
            while current_Mat[i+1][0] < parsed_tab[j][0] or current_Mat[i][0] >  parsed_tab[j][0]: #we want the following raw timestamp
                i = i+1
            growth = (current_Mat[i+1][i_feature]-current_Mat[i][i_feature]) / (current_Mat[i+1][0]-current_Mat[i][0])
            parsed_tab[j][i_feature] = current_Mat[i][i_feature] + (parsed_tab[j][0]-current_Mat[i][0]) * growth
    
    # Change into dataframe
    df = pd.DataFrame(parsed_tab, columns = cols)
    
    return df
    


def load_or_create_consumptiondf(load_path,timestep):
    path = os.getcwd()
    path = path.replace("\\","/")
    load_path = path + load_path
    suffix=load_path[-12:-4] #date
    save_name='consumption.'+str(timestep)+'min.csv'
    df_path = path+"/logAndCSV_GreEnEr/"+save_name
    try : #try to load
        consumption_df = pd.read_csv(df_path)
        print("consumption data successfully loaded !")
    except :
        #create
        with open(load_path) as f:
            txt=f.read()
            liste=txt.split("[[")
            txt='{"values": [['+liste[1]
        consumption_df = pd.DataFrame.from_dict(json.loads(txt), orient='columns')
        consumption_df = pd.DataFrame(consumption_df['values'].to_list(), columns=["timestamp", "Autres electricite", "Chauffage", "Eclairage", "Froid", "Prise de Courant", "Ventilation"])
        consumption_df = general_parser(consumption_df, timestep,'sec')
        #save
        consumption_df.to_csv(df_path, index_label=False, header=True)
        #load
        consumption_df = pd.read_csv(df_path)
        name_time = list(consumption_df.columns)[0]
        print("consumption data successfully parsed, saved and loaded !")
    return consumption_df








def merge_parser(X,Y,timestep=10, method='linear'):
    '''
    
    
    Parameters
    ----------
    X      : pandas.dataframe
        dataframe - proven compatibility if pandas dataframe
    X      : pandas.dataframe
        dataframe - proven compatibility if pandas dataframe
    timestep : int, optional
        Number of minute(s) you desire between timestamps (minimum : 1min)
    method : str, optional
        The interpolation method used. See link to pandas documentation below.
        https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.interpolate.html

    Returns
    -------
    df : pandas.dataframe
        Parsed data at selected timestep


    '''
    ### select time columns
    for col in X.columns:
        if col =='dt':
            dt_X = 'dt'
        if col == 'timestep':
            dt_X = 'timestep'
    for col in Y.columns:
        if col =='dt':
            dt_Y = 'dt'
        if col == 'timestep':
            dt_Y = 'timestep'
    ### cast time in int
    try:
        X[dt_X] = X[dt_X].apply(lambda x : x.timestamp())
        Y[dt_Y] = Y[dt_Y].apply(lambda x : x.timestamp())
    except:
        pass
    ## Select beginning and end - put dataframes to same limits
    min_ts = max( X[dt_X].min() , Y[dt_Y].min() )
    max_ts = min( X[dt_X].max() , Y[dt_Y].max() )
    print("\tmin_ts :",min_ts,"\n\tmax_ts:",max_ts)
    X = X [X.loc[:][dt_X] < max_ts ]
    X = X [X.loc[:][dt_X] > min_ts ]
    Y = Y [Y.loc[:][dt_Y] < max_ts ]
    Y = Y [Y.loc[:][dt_Y] > min_ts ]

    ### Merge dataframes and fill
    df = pd.concat([X, Y], ignore_index=True, sort=False)
    df.interpolate(method=method)
    
    ### Create dataframe with multiples of 10min
    minute_beginning  = (min_ts // timestep + 1)*timestep
    minute_ending     = (max_ts // timestep    )*timestep
    nb_ts        = int((minute_ending - minute_beginning) / timestep)
    parsed_tab = np.zeros(nb_ts)
    for i_ts in range(nb_ts):
        parsed_tab[i_ts] = minute_beginning + i_ts*timestep
    parsed_df = pd.DataFrame(parsed_tab, columns=['timestamp'])

    ### concatanate datasets
    df = pd.concat([df, parsed_df], ignore_index=True, sort=False)
    df.interpolate(method=method)
    df = df.query(' timestamp % (60*timestep) == 0')
    for col in df.columns:
        if col == 'dt':
            del df['dt']
    
    return df




