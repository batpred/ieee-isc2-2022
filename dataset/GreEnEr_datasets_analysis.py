# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 14:41:19 2021

@author: Louis Closson - louis.closson-professional@outlook.fr
"""

import pandas as pd
import os
import json
from pandas.io.json import json_normalize
import numpy as np
import matplotlib.pyplot as plt
import csv
import copy
from GreEnEr_dataset_defs import *

import scipy.cluster.hierarchy as hac
from scipy.cluster.hierarchy import fcluster
import scipy
import scipy.cluster.vq
import scipy.spatial.distance
dst = scipy.spatial.distance.euclidean


def monitor_parsing(df):
    #Using matrices to do pre-computations  - help during coding only
    new_mat = df.to_numpy()
    new_mat = new_mat[new_mat[:, 0].argsort()]  # sort by time (priority 2)
    if len(df.iloc[0]) > 2 and df.columns[1] == 'room':
        new_mat = new_mat[new_mat[:, 1].argsort(kind='mergesort')]  # sort by room (priority 1)
    dico_ts={}
    #followinf dictionnary must give only 2 values : dataset length and timestep(in millisec)
    for i in range(1,len(new_mat)):
        step = new_mat[i][0] - new_mat[i-1][0]
        if step not in dico_ts:
            dico_ts[step] = 1
        else :
            dico_ts[step]+=1
    return dico_ts




"""

Main script

"""


#TODO
#Change parameters
timestep = 10
feature = 2 # 2=temperatures ; 3=co2
merge_weather_data = True




########################### Load all data ###########################

# load/save weather aggregated dataframe
path=os.getcwd()
path=path.replace("\\","/")
#TODO
# ----Change the path (care to folder, "/" symbol and ".csv" type)----
df_path =  path + "/logAndCSV_GreEnEr/greener_weather."+str(timestep)+"min.csv"
try:
    weather_df = pd.read_csv(df_path)
except:
    # load/create weather dataframes
    load_path =  "/logAndCSV_GreEnEr/greener-dashboard-20211216.log"
    weather_df1 = load_or_create_weatherdf(load_path, timestep)
    load_path =  "/logAndCSV_GreEnEr/greener-dashboard-20220107.log"
    weather_df2 = load_or_create_weatherdf(load_path, timestep)
    load_path =  "/logAndCSV_GreEnEr/greener-dashboard.20220121.log"
    weather_df3 = load_or_create_weatherdf(load_path, timestep)
    load_path =  "/logAndCSV_GreEnEr/greener-dashboard.20220303.log"
    weather_df4 = load_or_create_weatherdf(load_path, timestep)
    # Aggregate 
    frames = [weather_df1, weather_df2, weather_df3, weather_df4]
    weather_df = pd.concat(frames, sort=False)
    weather_df = df_weather_parser(weather_df, timestep)
    #save
    weather_df.to_csv(df_path, index_label=False, header=True)
    print("weather datasets aggregated succesfully")

# load/save aggregated dataframe
path=os.getcwd()
path=path.replace("\\","/")
if feature == 2:
    suffix ='_temp'
if feature == 3:
    suffix = '_co2'

#TODO
# ----Change the path (care to folder, "/" symbol and ".csv" type)----
df_path =  path + "/logAndCSV_GreEnEr/greener_comfort"+suffix+"."+str(timestep)+"min.csv"
try:
    comfort_df = pd.read_csv(df_path)
except:
    # load/save comfort dataframes
    load_path="/logAndCSV_GreEnEr/greener-comfort.20220303.csv"
    comfort_df1 = load_or_create_comfortdf(load_path, timestep, feature=feature)
    load_path="/logAndCSV_GreEnEr/greener-comfort.20220121.csv"
    comfort_df2 = load_or_create_comfortdf(load_path, timestep, feature=feature)
    load_path="/logAndCSV_GreEnEr/greener-comfort-20220107.csv"
    comfort_df3 = load_or_create_comfortdf(load_path, timestep, feature=feature)
    load_path="/logAndCSV_GreEnEr/greener-comfort-20211216.csv"
    comfort_df4 = load_or_create_comfortdf(load_path, timestep, feature=feature)
    # Aggregate 
    frames = [comfort_df1, comfort_df2, comfort_df3, comfort_df4]
    comfort_df = pd.concat(frames, sort=False)
    comfort_df = general_parser(comfort_df, timestep)
    #save
    comfort_df.to_csv(df_path, index_label=False, header=True)
    print("comfort datasets aggregated succesfully")

# load energy json as dataframe
load_path = "/logAndCSV_GreEnEr/consumption.json"
consumption_df = load_or_create_consumptiondf(load_path,timestep)
        

########### correlation between weather data and sensors - ###########


### Select timestamps in the same moment (same beginning, same end)
min_ts = max(comfort_df['timestamp'].min(),weather_df['timestamp'].min(),consumption_df['timestamp'].min())
max_ts = min(comfort_df['timestamp'].max(),weather_df['timestamp'].max(),consumption_df['timestamp'].max())
comfort_df     = comfort_df    [comfort_df    .loc[:]['timestamp'] < max_ts ]
comfort_df     = comfort_df    [comfort_df    .loc[:]['timestamp'] > min_ts ]
weather_df     = weather_df    [weather_df    .loc[:]['timestamp'] < max_ts ]
weather_df     = weather_df    [weather_df    .loc[:]['timestamp'] > min_ts ]
consumption_df = consumption_df[consumption_df.loc[:]['timestamp'] < max_ts ]
consumption_df = consumption_df[consumption_df.loc[:]['timestamp'] > min_ts ]

### monitor the number of timestamps in datasets
dico_ts  = monitor_parsing(comfort_df    )
dico_ts2 = monitor_parsing(weather_df    )
dico_ts3 = monitor_parsing(consumption_df)

'''
    example : With 349 sensors (for 349 rooms) and a timestep of 10 min
    I get in dictionnaries the timestep 10*60000=600000
    If it appears x times in dico_ts2, then it appears x*349 times in dico_ts
    --> Then both dataset have the same number of samples
'''



### Convert to usable format for correlation matrix
Do_correlation=True
if Do_correlation:
    # Convert comfort dataframe into a matrix of series
    comfort_mat = comfort_df.to_numpy()
    # Convert weather dataframe to a matrix
    weather_mat = weather_df.to_numpy()
    # Convert energy dataframe to a matrix
    consumption_mat = consumption_df.to_numpy()
    # Correlation
    corr_mat_comfort_weather = np.corrcoef(comfort_mat, weather_mat    ,rowvar=False)
    corr_mat_comfort_energy  = np.corrcoef(comfort_mat, consumption_mat,rowvar=False)
    corr_mat_energie_weather = np.corrcoef(weather_mat, consumption_mat,rowvar=False)
    # Turn to dataframe to have headers
    cols = list(comfort_df.columns)+list(weather_df.columns)
    corr_df_comfort_weather     = pd.DataFrame(corr_mat_comfort_weather,cols)
    corr_df_comfort_weather.columns=cols
    
    cols = list(comfort_df.columns)+list(consumption_df.columns)
    corr_df_comfort_energy         = pd.DataFrame(corr_mat_comfort_energy,cols)
    corr_df_comfort_energy.columns=cols
    
    cols = list(weather_df.columns)+list(consumption_df.columns)
    corr_df_weather_consumption     = pd.DataFrame(corr_mat_energie_weather,cols)
    corr_df_weather_consumption.columns=cols

###################### Clustering ######################

### merge datasets
df = pd.merge(comfort_df, weather_df)
df = pd.merge(df,consumption_df)
Matrice_4_clustering = df.to_numpy()
Matrice_4_clustering = Matrice_4_clustering.T

# FOR PAPER ONLY - 1ST DATASET ONLY
#TODO
# ----Change the path (care to folder, "/" symbol and ".csv" type)----
load_path =  "/logAndCSV_GreEnEr/greener-dashboard-20220107.log"
weather_df = load_or_create_weatherdf(load_path, timestep)
load_path="/logAndCSV_GreEnEr/greener-comfort-20220107.csv"
comfort_df = load_or_create_comfortdf(load_path, timestep, feature=feature)
min_ts = max(comfort_df['timestamp'].min(),weather_df['timestamp'].min())
max_ts = min(comfort_df['timestamp'].max(),weather_df['timestamp'].max())
comfort_df     = comfort_df    [comfort_df    .loc[:]['timestamp'] < max_ts ]
comfort_df     = comfort_df    [comfort_df    .loc[:]['timestamp'] > min_ts ]
weather_df     = weather_df    [weather_df    .loc[:]['timestamp'] < max_ts ]
weather_df     = weather_df    [weather_df    .loc[:]['timestamp'] > min_ts ]
if merge_weather_data:
    df = pd.merge(comfort_df, weather_df)
else:
    df=comfort_df
Matrice_4_clustering = df.to_numpy()
Matrice_4_clustering = Matrice_4_clustering.T


### Standardize columns
means = np.mean(Matrice_4_clustering, axis=1)
means = np.reshape(means,[len(means),1])
std = np.std(Matrice_4_clustering, axis=1)
std = np.reshape(std,[len(std),1])
Matrice_4_clustering = np.divide( np.subtract(Matrice_4_clustering , means) ,  std)

labelList = list(df.columns)

# Convert comfort dataframe into a matrix of series
comfort_mat = comfort_df.to_numpy()
# Convert weather dataframe to a matrix
weather_mat = weather_df.to_numpy()
# Correlation
corr_mat_comfort_weather = np.corrcoef(comfort_mat, weather_mat    ,rowvar=False)
# Turn to dataframe to have headers
cols = list(comfort_df.columns)+list(weather_df.columns)
corr_df_comfort_weather     = pd.DataFrame(corr_mat_comfort_weather,cols)
corr_df_comfort_weather.columns=cols

### Show histograms - for correlation understanding over the building
# Idea : histograms may show clusters of rooms - with several gaussians for example
histogram_Min = corr_df_comfort_weather['tempMin'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and outdoor Minima")
plt.show()

histogram_Max = corr_df_comfort_weather['tempMax'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and outdoor Maxima")
plt.show()

histogram_Current = corr_df_comfort_weather['tempCurrent'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and outdoor current ones")
plt.show()

histogram_Direct = corr_df_comfort_weather['direct'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and direct light")
plt.show()

histogram_Diffuse = corr_df_comfort_weather['diffuse'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and diffuse light")
plt.show()

histogram_Global = corr_df_comfort_weather['global'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and global light")
plt.show()

histogram_timestamps = corr_df_comfort_weather['timestamp'].plot.hist(bins=25)
plt.title("Correlattion coefficients between rooms temperatures and timestamps")
plt.show()


### Do the clustering
print("start clustering")
Z = hac.linkage(Matrice_4_clustering, method='ward', metric='euclidean', optimal_ordering=True)
                                #tried method='ward' and method='single'
# Plot dendogram
if feature == 2:
    figsize = 150,60
if feature == 3:
    figsize = 20,6
plt.figure(figsize=(figsize)) #(20, 6) for CO2  / (40,10) (150,60) for temperatures - for nice dendogram
plt.title('Hierarchical Clustering Dendrogram')
plt.xlabel('sample index')
plt.ylabel('distance')
hac.dendrogram(
    Z,
    labels=labelList,
    leaf_rotation=90.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
)
plt.show()

T=np.arange(1,150,2)
cluster_Tab=[]
cluster_Tab_full=[]

# plot hierarchization graph
for i in T:
    cluster_Tab.append( (fcluster(Z, t=i, criterion='distance').max()) )
    cluster_Tab_full.append(  fcluster(Z, t=i, criterion='distance') )

fig, ax = plt.subplots(figsize=(10,5))
plt.title('Hierarchical Clustering levels')
plt.xlabel('distance')
plt.ylabel('number of clusters')
plt.plot(T, cluster_Tab)

#second plot may be usefull to visualize changes pace of aggregation
fig, ax = plt.subplots(figsize=(10,5))
plt.title('Hierarchical Clustering levels')
plt.xlabel('distance')
plt.ylabel('number of clusters')
plt.plot(T, cluster_Tab_full)


