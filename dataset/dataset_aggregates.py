# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 16:29:50 2022

@author: Louis Closson - louis.closson-professional@outlook.fr


This is a script to aggregate data over the CO2 and temperature columns,
create a dataframe with min, max, mean of both,
round timestamps to the nearest minute



"""

import pandas as pd
import os
import matplotlib.pyplot as plt
import scipy
import scipy.cluster.vq
import scipy.spatial.distance
dst = scipy.spatial.distance.euclidean


############################################################################
#
# Load a CSV file with : Timestamp, room, temperature, co2
# Create aggregated datasets for a feature, with: 
#       timestamp(as index)|min |mean|max   -> 2 features = 2 data sets
# Create aggregated datasets with min or max of a feature and the room:
#       timestamp(as index)|room|min OR max -> 2 features = 4 data sets
#
# As written :  provides a total of 6 data sets with aggregated data
#               for the 2 features temperature and co2
#
############################################################################




### CO2 and temperature over GreEn-Er rooms

path=os.getcwd()
path=path.replace("\\","/")
#path=path+"/greener comfort 20211216.log"
#path=path+"/greener comfort 20211216.csv"
path=path+"/greener comfort 20220107.csv"


#load dataframe from csv
comfort_df = pd.read_csv(path, header=None)

#put all lengths equal to the shortest
groups = comfort_df.groupby(comfort_df.columns[1]) #group by room
end_date = groups[0].max().min() #date of last timestamp to use

#cut data to end date
current_df = comfort_df[comfort_df.loc[:][0] <= end_date]
#remove malfunctionning co2 sensors
#current_df = current_df[current_df.loc[:][1] != "2D-003"] #easy to exclude on sight - high value
current_df = current_df[current_df.loc[:][1] != "5D-007"] #easy to exclude on sight - low value


#plot temperature OR CO2 time series - selected by 'feature'
fig, ax = plt.subplots(figsize=(125,50))
current_df.groupby(current_df.columns[1])[current_df.columns[3]].plot(legend=False)



#drop useless columns - no data on 01/21/2022
current_df = current_df.drop(columns=[x for x in comfort_df.columns if (x>3)])
#drop Nan - useless to aggregate
#current_df=current_df.dropna()


#aggregates data per timestamp
current_df = current_df.set_index(current_df.columns[1])
ts_groups = current_df.groupby(current_df.columns[0]) #group by timestamp
# minima:
ts_groups_min   = ts_groups.min() #minima over temperature and co2
rooms_of_minima = ts_groups.idxmin() #rooms of minima of temp and co2
minima          = rooms_of_minima.merge(ts_groups_min, left_index=True, right_index=True)
minima_temp  = minima.iloc[: , [0, 2]].copy()
minima_co2   = minima.iloc[: , [1, 3]].copy()
# maxima
ts_groups_max   = ts_groups.max()
rooms_of_maxima = ts_groups.idxmax() #rooms of minima of temp and co2
maxima          = rooms_of_maxima.merge(ts_groups_max, left_index=True, right_index=True)
maxima_temp  = maxima.iloc[: , [0, 2]].copy()
maxima_co2   = maxima.iloc[: , [1, 3]].copy()
#means:
ts_groups_mean = ts_groups.mean() #1 less column because mean(room) not computable

# #delete room column
# ts_groups_min = ts_groups_min.drop(columns=[1])
# ts_groups_max = ts_groups_max.drop(columns=[1])

#rename columns
ts_groups_min.rename(columns={2:"min_temp", 3:"min_co2"}, inplace=True)
ts_groups_mean.rename(columns={2:"mean_temp", 3:"mean_co2"}, inplace=True)
ts_groups_max.rename(columns={2:"max_temp", 3:"max_co2"}, inplace=True)

#fuse all dataframes
Agreg_df=ts_groups_min.merge(ts_groups_mean, left_index=True, right_index=True)
Agreg_df=Agreg_df.merge(ts_groups_max, left_index=True, right_index=True)

### round index to minute

#add index as a real column (to be able to round them later)
ts_df=Agreg_df.index.to_series()
Agreg_df=Agreg_df.merge(ts_df, left_index=True, right_index=True)
#rename it
Agreg_df.rename(columns={0:"timestamp"}, inplace=True)


#round timestamps to minute
def round_min(x):
    return (x//60)*60
Agreg_df['timestamp']=Agreg_df['timestamp'].apply(round_min)

#put timestamps as index
Agreg_df.set_index('timestamp', inplace=True)

#separation of temperature and co2 dataframes
cols = ['min_temp', 'mean_temp', 'max_temp']
temp_df = Agreg_df[cols]
cols = ['min_co2', 'mean_co2', 'max_co2']
co2_df = Agreg_df[cols]

#save both dataframes
temp_df.to_csv('temperature_20220107.csv')          #aggregation
minima_temp.to_csv('min_temperature_20220107.csv')  #min temp with rooms
maxima_temp.to_csv('max_temperature_20220107.csv')  #max temp with rooms

co2_df.to_csv('CO2_20220107.csv')                   #aggregation
minima_co2.to_csv('min_CO2_20220107.csv')           #min co2 with rooms
maxima_co2.to_csv('max_CO2_20220107.csv')           #max co2 with rooms


