# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 11:17:41 2022

@author: Louis CLOSSON - louis.closson-professional@outlook.fr
"""

import pandas as pd
import os
import json
import numpy as np
import matplotlib.pyplot as plt
import scipy.cluster.hierarchy as hac
from scipy.cluster.hierarchy import fcluster

import scipy
import scipy.cluster.vq
import scipy.spatial.distance
dst = scipy.spatial.distance.euclidean

#########################################################################
#
# For selected feature :
# Compute a clustering with scipy, and plot the dendogram.
# Plot temporal graphs for each room, with min, max and mean
#
#########################################################################


feature = 2 #  2:temperature,  3:co2
same_nb_sensors=False   #only keep rooms with co2 AND temperature sensors
                        #-> selecting '2' and 'True' will keep 74 sensors for temperature


#########################################################################


### CO2 and temperature over GreEn-Er rooms

path=os.getcwd()
path=path.replace("\\","/")
path=path+"/greener comfort 20220107.csv" #CSV in same folder as this code

#load dataframe from csv file in the same folder
comfort_df = pd.read_csv(path, header=None)

# #plot temperature and CO2 time series
# fig, ax = plt.subplots(figsize=(125,50))
# bp  = comfort_df.groupby(comfort_df.columns[1])[comfort_df.columns[2]].plot(legend=False)
# fig, ax = plt.subplots(figsize=(125,50))
# bp2 = comfort_df.groupby(comfort_df.columns[1])[comfort_df.columns[3]].plot(legend=False)


### Pre-processing part


#put all time series lengths equal to the shortest
groups = comfort_df.groupby(comfort_df.columns[1]) #group by room
end_date = groups[0].max().min() #date of last timestamp to use
current_df = comfort_df[comfort_df.loc[:][0] <= end_date] #cut data to end date

#remove malfunctionning co2 sensors
current_df = current_df[current_df.loc[:][1] != "2D-003"] #high  value outlier
current_df = current_df[current_df.loc[:][1] != "5D-007"] #low value outlier


fig, ax = plt.subplots(figsize=(125,50))
bp21 = current_df.groupby(current_df.columns[1])[current_df.columns[3]].plot(legend=False)


#drop useless columns - column with only NaN (last check : 01/31/2022)
current_df = current_df.drop(columns=[x for x in comfort_df.columns if (x>3) ])

#drop Nan left in columns of interest
if not same_nb_sensors:     # keep co2 OR temperature - chosen by 'feature'
    current_df = current_df.drop(columns=[x for x in current_df.columns if ((x>1) & (x!=feature)) ])
    current_df=current_df.dropna() #drop rows with NaN
else:                       # keep sensors giving temperature AND co2
    current_df=current_df.dropna() #drop rows with NaN
    current_df = current_df.drop(columns=[x for x in current_df.columns if ((x>1) & (x!=feature)) ])

#drop rows corresponding to time stamps where wanted data are lacking
nb_sensors   = len(current_df.groupby(current_df.columns[1]).size())
sensor_count =current_df.groupby(current_df.columns[0]).size()
min_sensors_count = sensor_count.min()
while min_sensors_count < nb_sensors :
    ts_to_remove = sensor_count[sensor_count.loc[:]==min_sensors_count].index[0] #timestamp lacking sensors -> will be removed
    print("removing a timestamp with", min_sensors_count," sensors instead of", nb_sensors)
    current_df=current_df[current_df.loc[:][0] != ts_to_remove]
    min_sensors_count = current_df.groupby(current_df.columns[0]).size().min()

#plot temperature OR CO2 time series - selected by 'feature'
fig, ax = plt.subplots(figsize=(125,50))
current_df.groupby(current_df.columns[1])[current_df.columns[-1]].plot(legend=False)

#calculation of timeseries duration
groups = current_df.groupby(current_df.columns[1]) #group by room
ts_length = groups.size().min() #number of timestamps per room - all equal now

#Using matrices to prepare dataset for clustering  
current_Mat = current_df.to_numpy()
current_Mat = current_Mat[current_Mat[:, 0].argsort()]  # sort by time (priority 2)
current_Mat = current_Mat[current_Mat[:, 1].argsort(kind='mergesort')]  # sort by room (priority 1)

# #Plotting only one room from the sorted list
# room_nb = 1 #0 to 350 with temperature sensors | 0 to 74 with co2 sensors
# room = current_Mat[ts_length*room_nb:ts_length*(room_nb+1),:] 
# plt.figure(figsize=(8,6))
# plt.plot(room[370:390,feature]) #from timestamp 0 to n-th timestand (index)
                            #feature if 'same_nb_sensors', 2 else

# transform into lists of time series
TimeSeries_dataset = []
for i in range(nb_sensors) :
    TimeSeries_dataset.append(current_Mat[ts_length*i:ts_length*(i+1),-1])
TimeSeries_dataset = np.array(TimeSeries_dataset)



### Clustering part



# Do the clustering
print("start clustering")
Z = hac.linkage(TimeSeries_dataset, method='ward', metric='euclidean', optimal_ordering=True)
                                #tried method='ward' and method='single'
# Plot dendogram
plt.figure(figsize=(15, 6)) #(15, 6) for CO2  /  (150,60) for temperatures - for nice dendogram
plt.title('Hierarchical Clustering Dendrogram')
plt.xlabel('sample index')
plt.ylabel('distance')
hac.dendrogram(
    Z,
    leaf_rotation=90.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels
)
plt.show()



# Clustering curve bases on distance needed to cluster - must refer to dendogram to select
# Strongly dependant on the dataset used - parameter for nice graph
#T=np.arange(1,30000,5)     #distance range for co2 - paper clustering - ward
#T=np.arange(1,13000,5)     #distance range for co2 - paper clustering - euclidean
T=np.arange(1,800,5)        #distance range for temperature - paper clustering - ward
#T=np.arange(1,350,5)       #distance range for temperature - paper clustering - euclidean
cluster_Tab=[]
cluster_Tab_full=[]

# plot hierarchization graph
for i in T:
    cluster_Tab.append( (fcluster(Z, t=i, criterion='distance').max()) )
    cluster_Tab_full.append(  fcluster(Z, t=i, criterion='distance') )

fig, ax = plt.subplots(figsize=(10,5))
plt.title('Hierarchical Clustering levels')
plt.xlabel('distance')
plt.ylabel('number of clusters')
plt.plot(T, cluster_Tab)

fig, ax = plt.subplots(figsize=(10,5))
plt.title('Hierarchical Clustering levels')
plt.xlabel('distance')
plt.ylabel('number of clusters')
plt.plot(T, cluster_Tab_full)


### Energy consumption distribution over GreEn-Er systems

path=os.getcwd()
path=path.replace("\\","/")
#path=path+"/GreEn-Er data.json"
path=path+"/GreEnEr_energy_data.json"


with open(path) as json_data:
    data = json.load(json_data)


df_names  = pd.DataFrame(data['names'])
MatNames  = np.array(df_names)
df_values = pd.DataFrame(data['values'], columns=MatNames)
MatValues = np.array(df_values)


### agregats - min, max, mean

ts_groups = current_df.groupby(current_df.columns[0]) #group by timestamp
ts_groups_min = ts_groups.min()
ts_groups_max = ts_groups.max()
ts_groups_mean = ts_groups.mean()

fig, ax = plt.subplots(figsize=(75,50))
current_df_ts   =current_df.set_index(current_df.columns[0], inplace=False)
current_df_ts.groupby(current_df_ts.columns[0])[current_df_ts.columns[-1]].plot(legend=False)
ts_groups_min [ts_groups_min.columns [-1]].plot(legend=False, c='blue', lw=12)
ts_groups_max [ts_groups_max.columns [-1]].plot(legend=False, c='red', lw=12)
ts_groups_mean[ts_groups_mean.columns[-1]].plot(legend=False, c='black', lw=15)

