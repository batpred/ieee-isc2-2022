# IEEE ISC2 2022 Dataset

Scripts for paper presented to IEEE ISC2 2022, 8th IEEE International Smart Cities Conference 2022, 26-29 September 2022, Aliathon Resort, Paphos, Cyprus https://attend.ieee.org/isc2-2022/

## Installation and configuration

TODO

## Datasets

The studies has been done on the [GreEn-ER building](https://ense3.grenoble-inp.fr/en/our-school/green-er-building) hosting the [Grenoble-INP Ense³ Engineering School](https://ense3.grenoble-inp.fr) and the [G2ELab (Grenoble Electrical Engineering Laboratory)](https://g2elab.grenoble-inp.fr/).

Delinchant B., Wurtz F., Ploix S., Schanen J.-L. and Marechal Y. (2016). GreEn-ER Living Lab - A Green Building with Energy Aware Occupants. SmartGreen'16, In proceedings of the 5th International Conference on Smart Cities and Green ICT Systems. ISBN 978-989-758-184-7, pages 316-323. DOI: 10.5220/0005795303160323

Martin Nascimento, Gustavo; Delinchant, Benoit; Wurtz, Frederic; Kuo-Peng, Patrick; Batistela, Nelson Jhoe; Laranjeira, Tiansi (2020), “GreEn-ER - Electricity Consumption Data of a Tertiary Building”, Mendeley Data, V1, doi: 10.17632/h8mmnthn5w.1 https://data.mendeley.com/datasets/h8mmnthn5w

The paper focuses mainly on the two live public datasets :
* [Confort data source](http://mhi-srv.g2elab.grenoble-inp.fr/API/comfort)
* [Dashboard data source](http://mhi-srv.g2elab.grenoble-inp.fr/API/dashboard)


Get the current value from [Confort data source](http://mhi-srv.g2elab.grenoble-inp.fr/API/comfort)
```bash
curl http://mhi-srv.g2elab.grenoble-inp.fr/API/comfort | jq .
```

Get the current value from [Dashboard data source](http://mhi-srv.g2elab.grenoble-inp.fr/API/dashboard)
```bash
curl http://mhi-srv.g2elab.grenoble-inp.fr/API/dashboard | jq .
```

## Running the scripts

TODO
